<?php declare(strict_types=1);

class Biedronka
{
    private $city;

    public function __construct(string $city)
    {
        $this->city = $city;
    }

    public function __toString()
    {
        return 'Biedronka znajduje się w: ' . $this->city;
    }
}

<?php declare(strict_types=1);

class Stokrotka
{
    private $city;

    public function __construct(string $city)
    {
        $this->city = $city;
    }

    public function __toString()
    {
        return 'Stokrotka znajduje sie w: ' . $this->city;
    }
}
<img alt="Logo" src="http://coderslab.pl/svg/logo-coderslab.svg" width="400">

# Funkcja autoload

>Wszystkie zadania rozwiązuj w przygotowanych do tego plikach

**WAŻNE -  nie zmieniaj struktury/nazw plików oraz korzystaj z przygotowanych zmiennych**

**Pamiętaj o typowaniu**

#### Zadanie 1 - rozwiązywane z wykładowcą

> Pliki do tego zadania znajdują się w folderze Zadanie1

Przeanalizuj kod w klasach User i Account. W pliku **zadanie1.php** stwórz po jednym obiekcie tych klas oraz wyświetl informację o nich. 

Klasy `User` oraz `Account` posiadają zdefiniowaną magiczną metodę `__toString()` dzięki niej możemy używać instrukcji `echo` na obiektach tych klas. Rezultatem będzie string **zwracany** przez metodę `__toString()` np:

```
$maciek = new User('maciek');
echo $maciek; // wyświetli "Imie: maciek"
```

Stwórz odpowiednią funkcje do automatycznego ładowania klas podczas tworzenia ich obiektów

#### Zadanie 2 - rozwiązywane z wykładowcą

> Pliki do tego zadania znajdują się w folderze Zadanie2

W pliku **zadanie2.php** stwórz i wyświetl informację o obiektach klas `Car`, `Bike` i `User`. Klasy `Bike` i `Car` znajdują się w folderze Vehicles natomiast klasa `User` znajduje się w folderze User.

Do zadania napisz dwie funkcje wczytujące pliki z odpowiednich folderów i zarejestruj je za pomocą funkcji `spl_autoload_register()`. 

Aby wyświetlić informację o obiekatch skorzystaj magicznej metody `__toString()`, która jest już zdefiniowana we wszystkich klasach.

**Uwaga**

Aby uniknąć warningów zanim wczytasz plik sprawdź czy istnieje on w danym folderze za pomocą funkcji [file_exists()](https://secure.php.net/manual/en/function.file-exists.php).

#### Zadanie 3

> Pliki do tego zadania znajdują się w folderze Zadanie3

W pliku **zadanie3.php** stwórz i wyświetl informację o obiektach klas `Biedronka`, `Stokrotka` i `Drink`. Klasy `Biedronka` i `Stokrotka` znajdują się w folderze Shops natomiast klasa `Drink` znajduje się w folderze Products.

Do wczytania klas użyj autoloadera wbudowanego w **Composer**

Aby wyświetlić informację o obiekatch skorzystaj magicznej metody `__toString()`, która jest już zdefiniowana we wszystkich klasach.

**Jak użyć autoloader Composera**
1. Będąc w katalogu zadania wykonaj polecenie `composer init`, potwierdź wszystkie kroki w generatorze.
2. Otwórz plik **composer.json** i dodaj w nim sekcję `autoload` (używaj formatu JSON).
3. W sekcji `autoload` umieść sekcje `classmap`. Dokładny opis jak dodać do niej foldery znajduje się w <a href="https://getcomposer.org/doc/04-schema.md#classmap">dokumentacji</a>.
3. Zainstaluj zależności composera za pomocą polecenia `composer install`.
4. Dołącz plik `vendor/autoload.php` do pliku z zadaniem.

**Uwaga**: Pamiętaj, że po każdej zmianie w pliku **composer.json** należy wykonać polecenie `composer dump-autoload`.

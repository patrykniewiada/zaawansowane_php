<img alt="Logo" src="http://coderslab.pl/svg/logo-coderslab.svg" width="400">

#  Maile

> Wszystkie zadania rozwiązuj w przygotowanych do tego plikach.

**WAŻNE -  nie zmieniaj struktury/nazw plików oraz korzystaj z przygotowanych zmiennych**# Zaawansowane PHP

**Pamiętaj o typowaniu**

#### Zadanie 1

1. Napisz stronę `kontakt` posiadającą formularz z następującymi polami:
   * imię i nazwisko,
   * adres email,
   * treść wiadomości.
2. Wypełniony formularz ma być przesyłany w postaci emaila na ustalony w kodzie adres.

Skorzystaj z biblioteki [PHPMailer][PHPMailer].

<!-- Links -->
[PHPMailer]: https://github.com/PHPMailer/PHPMailer

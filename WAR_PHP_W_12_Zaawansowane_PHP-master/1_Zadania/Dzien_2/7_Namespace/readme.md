<img alt="Logo" src="http://coderslab.pl/svg/logo-coderslab.svg" width="400">

# Namespace

>Wszystkie zadania rozwiązuj w przygotowanych do tego plikach

**WAŻNE -  nie zmieniaj struktury/nazw plików oraz korzystaj z przygotowanych zmiennych**

**Pamiętaj o typowaniu**

#### Zadanie 1 - rozwiązywane z wykładowcą

> Pliki do tego zadania znajdują się w folderze Zadanie1

1. W folderze **Zadanie1** stwórz plik **User.php** i utwórz w nim klasę `User`, nadaj jej namespace `Zadanie1`. 
2. W folderze **Interfaces** znajdują się 4 interface'y:
    * `GoodbyeInterface` z metodą `sayGoodbye()`, która ma wyświetlać napis `Bye Bye!`
    * `HelloInterface` z metodą `sayHello()`, która ma wyświetlać napis `Hello!`
    * `WatchInterface` z metodą `watch()`, która ma wyświetlać napisz `I'm watching You!`
    * `IntroductionInterface` z metodami: `sayYourNamespace()`, która ma wyświetlić namespace obiektu oraz `sayYourClass`, która ma wyświetlić klasę obiektu
3. W stworzonej w punkcie 1 klasie `User` zaimplementuj interface'y `IntroductionInterface` i `HelloInterface`. W pliku **zadanie1.php** utwórz obiekt tej klasy i wywołaj metody zaimplementowanych interface'ów.
4. W folderze **Zadanie1** stwórz folder **User** a w nim klasę `User`, nadaj jej namespace `Zadanie1\User`. 
5. W stworznej w punkcie 4 klasie `User` zaimplementuj interface'y `IntroductionInterface` i `GoodbyeInterface`. W pliku **zadanie1.php** utwórz obiekt tej klasy i wywołaj metody zaimplementowanych interface'ów.
6. W stworzonym w punkcie 4 folderze **User** stwórz klasę `Watcher`, nadaj jej namespace `Zadanie1\User`. 
7. W stworznej w punkcie 6 klasie `Watcher` zaimplementuj interface'y `IntroductionInterface` i `WatchInterface`. W pliku **zadanie1.php** utwórz obiekt tej klasy i wywołaj metody zaimplementowanych interface'ów.

#### Zadanie 2

Zmień plik **zadanie1.php** tak aby przy tworzeniu obiektów poszczególnych klas nie podawać pełnych namespace'ów. W tym celu użyj instrukcji `use` oraz skorzystaj z aliasów.

#### Zadanie 3

W pliku **zadanie1.php** usuń instrukcje `require`. Napisz autoloader wczytujący klasy z ich namespace'ami.

Użyj instrukcji `__DIR__` oraz funkcji `str_replace` aby zamienić namespace na ścieżkę pliku.

#### Zadanie 4

> Pliki do tego zadania znajdują się w folderze Zadanie4

W katalogu **Zadanie4** znajduje się szkielet aplikacji. Plik wykonujący kod znajduje się w pliku **zadanie4.php** natomiast w folderze **src** znajdują się klasy z namespece'ami.

Do pliku **zadanie4.php** dodaj autoloader wbudowany w composer, tak by klasy zostały poprawnie wczytane.

**Jak użyć autoloader composera**
1. Będąc w katalogu zadania wykonaj polecenie `composer init`, potwierdź wszystkie kroki w generatorze.
2. Otwórz plik **composer.json** i dodaj w nim sekcję `autoload` (używaj formatu JSON).
3. W sekcji `autoload` umieść sekcje `psr-4`. Dodaj w niej nazwy namespace'ów i ścieżki do klas. Przykład:
    ```json
    "autoload":{
       "psr-4": {
           "nazwa\\namespace'a\\": "sciezka/klasy"
      }
    }
    ```

    Dokładny opis jak dodać do niej foldery znajduje się w [dokumentacji](https://getcomposer.org/doc/04-schema.md#psr-4).
    
4. Zainstaluj zależności composera za pomocą polecenia `composer install`.
5. Dołącz plik `vendor/autoload.php` do pliku z zadaniem.

**Uwaga**: Pamiętaj, że po każdej zmianie w pliku **composer.json** należy wykonać polecenie `composer dump-autoload`.

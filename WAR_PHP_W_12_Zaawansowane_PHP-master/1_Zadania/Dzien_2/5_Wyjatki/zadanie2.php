<?php declare(strict_types=1);

function divide(int $divider, int $dividend): float
{
    if ($divider == 0) {
        throw new InvalidArgumentException("Divide by zero error");
    }

    if ($divider < 0) {
        throw new OutOfRangeException ("Divide is lover then zero");
    }

    return $dividend / $divider;
}

function randomDivide(int $tryNumber): void
{
    for ($n = 0; $n < $tryNumber; $n++) {
        echo divide(mt_rand(0, 20) === 0 ? 0 : 5, rand(-10, 10)) . '</br>';
    }
}